package helpers;

import lib.Init;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import ru.yandex.qatools.allure.Allure;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.events.MakeAttachmentEvent;
import ru.yandex.qatools.allure.events.StepFinishedEvent;
import ru.yandex.qatools.allure.events.StepStartedEvent;

public class AllureHelper {
    private AllureHelper() {
    }

    public static void toAllure(String message, String value) {
        Allure.LIFECYCLE.fire(new StepStartedEvent(message + " : " + value));
        Allure.LIFECYCLE.fire(new StepFinishedEvent());
    }

    public static void addAttachment(byte[] attachment, String title, String type) {
        Allure.LIFECYCLE.fire(new MakeAttachmentEvent(attachment, title, type));
    }

    @Attachment(value = "screenshot", type = "img/png")
    public static byte[] takeScreenshot() {
        return ((TakesScreenshot) Init.getDriver()).getScreenshotAs(OutputType.BYTES);
    }
}

