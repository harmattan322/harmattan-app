package pages;

import lib.Init;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by DadaL1fe on 20.05.2016.
 */
public abstract class AnyPage {
    public AnyPage() {
        PageFactory.initElements(Init.getDriver(), this); // инициализируем элементы
        waitPageToLoad();
    }

    public void waitPageToLoad() {
        new WebDriverWait(Init.getDriver(), 30).until((ExpectedCondition<Boolean>) wd ->
                ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
    }

    public void click(WebElement element) {
        new WebDriverWait(Init.getDriver(), 30)
                .until(ExpectedConditions.elementToBeClickable(element));
        System.out.println("Click to " + element.getText());
        element.click();
    }

    public void setText(WebElement element, String text) {
        element.clear();
        element.sendKeys(text);
    }
}
