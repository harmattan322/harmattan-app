package pages;

import lib.Init;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static helpers.AllureHelper.addAttachment;
import static helpers.AllureHelper.takeScreenshot;
import static helpers.AllureHelper.toAllure;

/**
 * Created by DadaL1fe on 27.05.2016.
 */
public class DnsPage extends AnyPage {

    @FindBy(xpath = "//span[contains(text(),'Цвет')]/..")
    private WebElement listboxColor;


    @FindBy(xpath = "//span[contains(text(),'Совместимость')]/..")
    private WebElement listboxCompatibility;

    @FindBy(xpath = "//span[contains(text(),'Ресурс')]/..")
    private WebElement listboxResource;

    @FindBy(xpath = "//span[text()='Ресурс']/following::input[contains(@id, '_from')]")
    private WebElement onListboxResourceFrom;

    @FindBy(xpath = "//span[text()='Ресурс']/following::input[contains(@id, '_to')]")
    private WebElement onListboxResourceTo;

    @FindBy(xpath = "//span[contains(text(),'Количество в упаковке')]/..")
    private WebElement listboxCountInPackage;

    @FindBy(xpath = "//span[contains(text(),'Поддерживаемые модели принтеров')]/..")
    private WebElement listboxValidPrinters;

    @FindBy(xpath = "//button[text()='Показать']")
    private WebElement buttonShow;

    @FindBys(@FindBy(xpath = "//div[@data-id='catalog-item']//span[contains(text(), '[')]"))
    private List<WebElement> listItemsDesc;

    @FindBys(@FindBy(xpath = "//div[@data-id='catalog-item']//div[@class='title']/a/h3"))
    private List<WebElement> listItemsName;

    @FindBys(@FindBy(xpath = "//span[@class='avail-text soft-avails']/a/span"))
    private List<WebElement> listExistence;

    private BigDecimal roundUp(double value) {
        return new BigDecimal("" + value).setScale(0, BigDecimal.ROUND_HALF_UP); //метод округления
    }


    public DnsPage() {
        new WebDriverWait(Init.getDriver(), 30)
                .until(ExpectedConditions.presenceOfElementLocated
                        (By.xpath("//h1[text()='Картриджи лазерные']")));
        PageFactory.initElements(Init.getDriver(), this);
    }

    public void pickManufacture() {
        try {
            if (Init.getStash().get("st_manufacture") != null) {
                for (int i = 0; i < Init.getStash().get("st_manufacture").size(); i++) {
                    ((JavascriptExecutor) Init.getDriver()).executeScript("element = document.evaluate(\""
                            + "//nav[@id='header-search']" + "\", document, null, XPathResult.ANY_TYPE, null)"
                            + ".iterateNext();if (element !== null) {element.parentNode.removeChild(element);};");
                    WebElement elem = Init.getDriver().findElement(By.xpath("//label[contains(text(),'"
                            + Init.getStash().get("st_manufacture").get(i) + "')]"));
                    ((JavascriptExecutor) Init.getDriver()).executeScript("arguments[0].scrollIntoView(true);", elem);
                    elem.click();
                    toAllure("Производитель", Init.getStash().get("st_manufacture").get(i));
                }
            }
        } catch (NullPointerException e) {
            toAllure("Производитель", "не задан");
            e.printStackTrace();
            Assert.fail();
        }
    }

    public void pickColor() {
        try {
            if (Init.getStash().get("st_color") != null) {
                click(listboxColor);
                for (int i = 0; i < Init.getStash().get("st_color").size(); i++) {
                    ((JavascriptExecutor) Init.getDriver()).executeScript("element = document.evaluate(\""
                            + "//nav[@id='header-search']" + "\", document, null, XPathResult.ANY_TYPE, null)"
                            + ".iterateNext();if (element !== null) {element.parentNode.removeChild(element);};");
                    WebElement elem = Init.getDriver().findElement(By.xpath("//label[contains(text(),'"
                            + Init.getStash().get("st_color").get(i) + "')]"));
                    click(elem);
                    toAllure("Цвет", Init.getStash().get("st_color").get(i));
                }
            }
        } catch (NullPointerException e) {
            toAllure("Цвет", "не задан");
            e.printStackTrace();
            Assert.fail();
        }
    }

    public void pickCompatibility() {
        try {
            if (Init.getStash().get("st_compatibility") != null) {
                click(listboxCompatibility);
                for (int i = 0; i < Init.getStash().get("st_compatibility").size(); i++) {
                    WebElement elem = Init.getDriver().findElement(By.xpath("//label[contains(text(),'"
                            + Init.getStash().get("st_compatibility").get(i) + "')]"));
                    click(elem);
                    toAllure("Совместимость", Init.getStash().get("st_compatibility").get(i));
                }
            }
        } catch (NullPointerException e) {
            toAllure("Совместимость", "не задана");
            e.printStackTrace();
            Assert.fail();
        }
    }

    public void pickResources() {
        try {
            if (Init.getStash().get("st_resource") != null) {
                click(listboxResource);
                click(onListboxResourceFrom);
                setText(onListboxResourceFrom, Init.getStash().get("st_resource").get(0));
                toAllure("Значение ресурса ОТ", Init.getStash().get("st_resource").get(0));
                if (Init.getStash().get("st_resource").size() > 1) {
                    click(onListboxResourceTo);
                    setText(onListboxResourceTo, Init.getStash().get("st_resource").get(1));
                    toAllure("Значение ресурса ДО", Init.getStash().get("st_resource").get(1));
                }
            }
        } catch (NullPointerException e) {
            toAllure("Значение ресурса", "не задано");
            e.printStackTrace();
            Assert.fail();
        }
    }

    public void pickCountInPackage() {
        try {
            if (Init.getStash().get("st_countInPackage") != null) {
                click(listboxCountInPackage);
                for (int i = 0; i < Init.getStash().get("st_countInPackage").size(); i++) {
                    WebElement elem = Init.getDriver().findElement(By.xpath("//label[contains(text(),'"
                            + Init.getStash().get("st_countInPackage").get(i) + "')]"));
                    click(elem);
                    toAllure("Количество в упаковке", Init.getStash().get("st_countInPackage").get(i));
                }
            }
        } catch (NullPointerException e) {
            toAllure("Количество в упаковке", "не задано");
            e.printStackTrace();
            Assert.fail();
        }
    }

    public void pickValidPrinters() {
        try {
            if (Init.getStash().get("st_validPrinters") != null) {
                click(listboxValidPrinters);
                for (int i = 0; i < Init.getStash().get("st_validPrinters").size(); i++) {
                    ((JavascriptExecutor) Init.getDriver()).executeScript("element = document.evaluate(\""
                            + "//nav[@id='header-search']" + "\", document, null, XPathResult.ANY_TYPE, null)"
                            + ".iterateNext();if (element !== null) {element.parentNode.removeChild(element);};");
                    WebElement elem = Init.getDriver().findElement(By.xpath("//label[contains(text(),'"
                            + Init.getStash().get("st_validPrinters").get(i) + "')]"));
                    ((JavascriptExecutor) Init.getDriver()).executeScript("arguments[0].scrollIntoView();", elem);
                    click(elem);
                    toAllure("Поддерживаемые принтеры", Init.getStash().get("st_validPrinters").get(i));
                }
            }
        } catch (NullPointerException e) {
            toAllure("Поддерживаемые принтеры", "не задано");
            e.printStackTrace();
            Assert.fail();
        }
    }

    public void pushButtonShow() {
        click(buttonShow);
        toAllure("Успешное нажатие", "кнопка Показать");
        waitPageToLoad();
        new WebDriverWait(Init.getDriver(), 15).until(ExpectedConditions.invisibilityOfAllElements(Init.getDriver()
                .findElements(By.xpath("//div[@class='loader']/*[@class='circular']"))));
    }

    public void checkName() {
        try {
            int countNameCheck = 0;
            if (Init.getStash().get("st_manufacture") != null) {
                for (WebElement element : listItemsName) {
                    for (int i = 0; i < Init.getStash().get("st_manufacture").size(); i++) {
                        if (element.getText().contains(Init.getStash().get("st_manufacture").get(i))) {
                            toAllure("Успешная проверка  имени товара №" + ++countNameCheck,
                                    Init.getStash().get("st_manufacture").get(i));
                        }
                    }
                }
            }
        } catch (NullPointerException e) {
            toAllure("Проверяем имя каждого товара", "товары не заданы");
            Assert.fail();
        }
    }

    public void checkDesc() {
        int countColorCheck = 0;
        int countCompatibilityCheck = 0;
        int countResourceCheck = 0;
        int countCountInPackage = 0;
        new WebDriverWait(Init.getDriver(), 15).until(ExpectedConditions.visibilityOfElementLocated(By.
                xpath("//div[@data-id='catalog-item']//span[contains(text(), '[')]")));
        for (WebElement element : listItemsDesc) {
            String textOfItemDesc = element.getText();
            try {
                if (Init.getStash().get("st_color") != null) {
                    for (int i = 0; i < Init.getStash().get("st_color").size(); i++) {
                        if (textOfItemDesc.contains(Init.getStash().get("st_color").get(i))) {
                            toAllure("Успешная проверка  цвета товара №"
                                    + ++countColorCheck, Init.getStash().get("st_color").get(i));
                        }
                    }
                }
            } catch (NullPointerException e) {
                toAllure("Проверяем цвет товаров", "не задан");
                Assert.fail();
            }
            try {
                if (Init.getStash().get("st_compatibility") != null) {
                    for (int i = 0; i < Init.getStash().get("st_compatibility").size(); i++) {
                        if (textOfItemDesc.contains(Init.getStash().get("st_compatibility").get(i))) {
                            toAllure("Успешная проверка  совместимости товара №" + ++countCompatibilityCheck,
                                    Init.getStash().get("st_compatibility").get(i));
                        }
                    }
                }
            } catch (NullPointerException e) {
                toAllure("Проверяем совместимость товара", "не задана");
                Assert.fail();
            }
            try {
                if (Init.getStash().get("st_resource") != null) {
                    Pattern pattern = Pattern.compile("\\d+");
                    Matcher matcher = pattern.matcher(textOfItemDesc);
                    if (Init.getStash().get("st_resource").get(0).isEmpty() || Init.getStash().get("st_resource")
                            .get(1).isEmpty()) {
                        click(listboxResource);
                        if (Init.getStash().get("st_resource").get(0).isEmpty() && !Init.getStash().get("st_resource")
                                .get(1).isEmpty()) {
                            if (matcher.find()) {
                                Assert.assertTrue("Resource is not valid", Integer.parseInt(matcher.group())
                                        <= Integer.parseInt(Init.getStash().get("st_resource").get(1)));
                                toAllure("Успешная проверка значения ресурса у товара товара ДО №"
                                        + ++countResourceCheck, Init.getStash().get("st_resource").get(1));
                            }
                        } else if (!Init.getStash().get("st_resource").get(0).isEmpty() && Init.getStash()
                                .get("st_resource").get(1).isEmpty()) {
                            if (matcher.find()) {
                                Assert.assertTrue("Resource is not valid", Integer.parseInt(matcher.group())
                                        >= Integer.parseInt(Init.getStash().get("st_resource").get(0)));
                                toAllure("Успешная проверка значения ресурса у товара товара ОТ №"
                                        + ++countResourceCheck, Init.getStash().get("st_resource").get(0));
                            }
                        } else if (Init.getStash().get("st_resource").get(0).isEmpty() && Init.getStash()
                                .get("st_resource").get(1).isEmpty()) {
                            String numberResourceFrom = onListboxResourceFrom.getAttribute("placeholder");
                            String numberResourceTo = onListboxResourceTo.getAttribute("placeholder");
                            Assert.assertTrue("Resource is not valid", Integer.parseInt(numberResourceFrom)
                                    <= Integer.parseInt(numberResourceTo));
                            toAllure("Успешная проверка значений ресурса у товара товара ОТ и ДО №"
                                    + ++countResourceCheck, numberResourceFrom + numberResourceTo);
                        }
                    } else {
                        if (matcher.find()) {
                            System.out.println("Ресурс: " + matcher.group());
                            Assert.assertTrue("Resource is not valid", Integer.parseInt(Init.getStash()
                                    .get("st_resource").get(0))
                                    <= Integer.parseInt(matcher.group()) && Integer.parseInt(matcher.group())
                                    <= Integer.parseInt(Init.getStash().get("st_resource").get(1)));
                            toAllure("Успешная проверка значений ресурса у товара ОТ и ДО №"
                                    + ++countResourceCheck, Init.getStash().get("st_resource").get(0)
                                    + " - " + Init.getStash().get("st_resource").get(1));
                        }
                    }
                }
            } catch (NullPointerException e) {
                toAllure("Проверяем значение ресурса", "не задано");
                Assert.fail();
            } catch (ArrayIndexOutOfBoundsException e1) {
                System.out.println("OVER 9000 ARRAY RESOURCE!!");
                Assert.fail();
            }
            try {
                if (Init.getStash().get("st_countInPackage") != null) {
                    for (int i = 0; i < Init.getStash().get("st_countInPackage").size(); i++) {
                        if (textOfItemDesc.contains(Init.getStash().get("st_countInPackage").get(i))) {
                            toAllure("Успешная проверка на кол-во в упаковке товара № " + ++countCountInPackage,
                                    Init.getStash().get("st_countInPackage").get(i));
                        }
                    }
                }
            } catch (NullPointerException e) {
                toAllure("Проверяем кол-во в упаковке", "не задано");
                Assert.fail();
            }

        }
    }

    public void findAverage() {
        try {
            int countSize = 0;

            for (WebElement aListExistence : listExistence) {
                String textOfElem = aListExistence.getText();
                if (!textOfElem.startsWith("через") || !textOfElem.startsWith("завтра")) {
                    countSize++;
                }
            }
            int average = Integer.parseInt(String.valueOf(roundUp(((double) countSize / 2.0))));
            System.out.println("average SOLVING: " + average);
            WebElement averagePrice = listExistence.get(average).findElement(
                    By.xpath("./../../../../..//span[@data-of='price-total']"));
            WebElement averageGood = listExistence.get(average).findElement(
                    By.xpath("./../../../../..//div[@class='title']/a/h3"));
            System.out.println("Товар: " + averageGood.getText() + " в наличии со средней ценой: "
                    + averagePrice.getText());
            toAllure("Товар: " + averageGood.getText(), " в наличии со средней ценой: " + averagePrice.getText());
        } catch (NullPointerException e) {
            toAllure("Товар в наличии со средней ценой", "товаров не найдено");
            Assert.fail();
        }
    }


    public void findCheap() {
        try {
            Pattern pattern = Pattern.compile(".*\\d+.*");
            WebElement currentElem;
            WebElement elemWithMinPrice = null;
            String nameOfElemWithMinPrice = null;
            int price;
            int minPrice = 0;
            String optionOfDelivery = null;
            for (WebElement aListExistence : listExistence) {

                Matcher matcher = pattern.matcher(aListExistence.getText());
                while (matcher.find()) {
                    optionOfDelivery = matcher.group();
                    if (aListExistence.getText().contains(optionOfDelivery)) {

                        WebElement elemPrice = aListExistence.findElement(By.xpath
                                ("./../../../../..//span[@data-of='price-total']"));
                        price = Integer.parseInt(elemPrice.getText().replaceAll(" ", ""));
                        currentElem = elemPrice.findElement(By.xpath("./../../../../..//div[@class='title']/a/h3"));
                        if (nameOfElemWithMinPrice == null || minPrice == 0 || price < minPrice) {
                            minPrice = price;
                            elemWithMinPrice = currentElem;
                            nameOfElemWithMinPrice = currentElem.getText();
                        }
                        break;
                    }
                }
            }
            if (nameOfElemWithMinPrice != null) {
                System.out.println("Товар с наим. ценой на ближайший заказ: " + nameOfElemWithMinPrice
                        + ", цена: " + minPrice + ", " + optionOfDelivery);
                toAllure("Товар: " + nameOfElemWithMinPrice, " с наименьшей ценой на ближайший заказ: "
                        + minPrice + ", " + optionOfDelivery);
                click(elemWithMinPrice);
                addAttachment(takeScreenshot(), "Result", "image/png");
            }

        } catch (NullPointerException e) {
            toAllure("Ищем товар с наименьшей ценой на ближайший заказ", "товаров не найдено");
            Assert.fail();
            e.printStackTrace();
        }
    }
}
