package stepDefinitions;


import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import lib.Init;
import pages.DnsPage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import static helpers.AllureHelper.toAllure;

/**
 * Created by DadaL1fe on 18.05.2016.
 */
public class CommonStepDefinitions {
    @Before
    public void loadProperty() {
        FileInputStream beforeInput = null;
        try {
            beforeInput = new FileInputStream("src/test/java/config/application.properties");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Properties prop = new Properties();
        try {
            prop.load(beforeInput);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            String manufacture = prop.getProperty("Manufacture");
            List<String> listManufacture = Arrays.asList(manufacture.split(";"));
            Init.setStash("st_manufacture", listManufacture);
        } catch (NullPointerException e1) {
            System.out.println("Проперти, отсутствует значение Manufacture");
            toAllure("Проперти", "отсутствует значение Manufacture");
        }

        try {
            String color = prop.getProperty("Color");
            List<String> listColor = Arrays.asList(color.split(";"));
            Init.setStash("st_color", listColor);
        } catch (NullPointerException e1) {
            System.out.println("Проперти, отсутствует значение Color");
            toAllure("Проперти", "отсутствует значение Color");
        }

        try {
            String compatibility = prop.getProperty("Compatibility");
            List<String> listCompatibility = Arrays.asList(compatibility.split(";"));
            Init.setStash("st_compatibility", listCompatibility);
        } catch (NullPointerException e1) {
            System.out.println("Проперти, отсутствует значение Compatibility");
            toAllure("Проперти", "отсутствует значение Compatibility");
        }

        try {
            String resources = prop.getProperty("Resource");
            List<String> listResources = Arrays.asList(resources.split(";"));
            Init.setStash("st_resource", listResources);
        } catch (NullPointerException e1) {
            System.out.println("Проперти, отсутствует значение Resource");
            toAllure("Проперти", "отсутствует значение Resource");
        }

        try {
            String countInPackage = prop.getProperty("CountInPackage");
            List<String> listCountInPackage = Arrays.asList(countInPackage.split(";"));
            Init.setStash("st_countInPackage", listCountInPackage);
        } catch (NullPointerException e1) {
            System.out.println("Проперти, отсутствует значение Count In Package");
            toAllure("Проперти", "отсутствует значение Count In Package");
        }

        try {
            String validPrinters = prop.getProperty("ValidPrinters");
            List<String> listValidPrinters = Arrays.asList(validPrinters.split(";"));
            Init.setStash("st_validPrinters", listValidPrinters);
        } catch (NullPointerException e1) {
            System.out.println("Проперти, отсутствует значение Valid Printers");
            toAllure("Проперти", "отсутствует значение Valid Printers");
        }

        try {
            String url = prop.getProperty("URL");
            List<String> listURL = Arrays.asList(url.split(";"));
            Init.setStash("st_URL", listURL);
        } catch (NullPointerException e1) {
            e1.printStackTrace();
        }

        try {
            String browser = prop.getProperty("Browser");
            List<String> listBrowser = Arrays.asList(browser.split(";"));
            Init.setStash("st_browser", listBrowser);
        } catch (NullPointerException e1) {
            e1.printStackTrace();
        }

        try {
            String startDate = prop.getProperty("StartDate");
            List<String> listStartDate = Arrays.asList(startDate.split(";"));
            Init.setStash("st_startDate", listStartDate);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        try {
            String finishDate = prop.getProperty("FinishDate");
            List<String> listFinishDate = Arrays.asList(finishDate.split(";"));
            Init.setStash("st_finishDate", listFinishDate);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        try {
            String totalCost = prop.getProperty("TotalCost");
            List<String> listTotalCost = Arrays.asList(totalCost.split(";"));
            Init.setStash("st_totalCost", listTotalCost);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        try {
            String totalCostAfterClickEnough = prop.getProperty("TotalCostAfterClickEnough");
            List<String> listTotalCostAfterClickEnough = Arrays.asList(totalCostAfterClickEnough.split(";"));
            Init.setStash("st_totalCostAfterClickEnough", listTotalCostAfterClickEnough);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        try {
            String totalCostAfterClickSport = prop.getProperty("TotalCostAfterClickSport");
            List<String> listTotalCostAfterClickSport = Arrays.asList(totalCostAfterClickSport.split(";"));
            Init.setStash("st_totalCostAfterClickSport", listTotalCostAfterClickSport);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        try {
            String costSport = prop.getProperty("CostSport");
            List<String> listCostSport = Arrays.asList(costSport.split(";"));
            Init.setStash("st_costSport", listCostSport);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        try {
            String totalCostWithoutBaggage = prop.getProperty("TotalCostWithoutBaggage");
            List<String> listTotalCostWithoutBaggage = Arrays.asList(totalCostWithoutBaggage.split(";"));
            Init.setStash("st_totalCostWithoutBaggage", listTotalCostWithoutBaggage);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        try {
            String totalCostWithBaggageCautious = prop.getProperty("TotalCostWithBaggageCautious");
            List<String> listTotalCostWithBaggageCautious = Arrays.asList(totalCostWithBaggageCautious.split(";"));
            Init.setStash("st_totalCostWithBaggageCautious", listTotalCostWithBaggageCautious);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Given("^Go to link$")
    public void goToLink() {
        toAllure("Переход на страницу ", Init.getStash().get("st_URL").get(0));
        Init.getDriver().get(Init.getStash().get("st_URL").get(0));
    }

    @Given("^pick Criteria$")
    public void pickCriteria() {
        DnsPage dnsPage = new DnsPage();
        dnsPage.pickManufacture();
        dnsPage.pickColor();
        dnsPage.pickCompatibility();
        dnsPage.pickResources();
        dnsPage.pickCountInPackage();
        dnsPage.pickValidPrinters();
    }

    @Given("^push Button$")
    public void pushButton() {
        DnsPage dnsPage = new DnsPage();
        dnsPage.pushButtonShow();
    }

    @Given("^check Name$")
    public void checkName() {
        DnsPage dnsPage = new DnsPage();
        dnsPage.checkName();
    }

    @Given("^check Desc$")
    public void checkDesc() {
        DnsPage dnsPage = new DnsPage();
        dnsPage.checkDesc();
    }

    @Given("^find Average Good$")
    public void findAverage() {
        DnsPage dnsPage = new DnsPage();
        dnsPage.findAverage();
    }

    @Given("^find Cheap Goods with Waiting$")
    public void findCheap() {
        DnsPage dnsPage = new DnsPage();
        dnsPage.findCheap();
    }

    @After
    public void quit() {
        Init.clearStash();
        Init.getDriver().quit();
    }
}
